from dataclasses import dataclass, asdict
import json
from peewee import (
    SqliteDatabase,
    Model,
    PrimaryKeyField,
    CharField,
    FloatField,
    TextField,
    IntegerField,
    DateTimeField,
    OperationalError,
)
from datetime import datetime
from typing import List
from model import Product

db = SqliteDatabase("remarkable.db")


class _BaseModel(Model):
    class Meta:
        database = db


class ProductDTO(_BaseModel):
    id = PrimaryKeyField()
    title = CharField()
    price = FloatField()
    image = TextField()
    description = CharField()
    category = CharField()
    rating = FloatField()
    rating_count = IntegerField()


class ProductStorage:
    def __init__(self):
        self.db = db
        self.db.connect()
        self.db.create_tables([ProductDTO])

    def get_products(self) -> List[Product]:
        product_dtos = ProductDTO.select().dicts()
        return [Product(**(p)) for p in product_dtos]

    def save_products(self, products: List[Product]):
        product_dicts = [asdict(p) for p in products]
        ProductDTO.insert_many(product_dicts).on_conflict_replace().execute()
