from db import ProductStorage

import pandas as pd

if __name__ == "__main__":
    product_stogare = ProductStorage()
    products = product_stogare.get_products()

    df = pd.DataFrame(products)
    # df.info()
    df = pd.get_dummies(columns=["category"], data=df)
    df = df.rename(
        columns=lambda x: x.replace("_", " ")
        .replace("'", " ")
        .strip()
        .replace(" ", "_")
    )

    print(df)
