from dataclasses import dataclass


@dataclass
class Product:
    id: int
    title: str
    price: float
    image: str
    description: str
    category: str
    rating: float
    rating_count: int
