from dataclasses import dataclass
from typing import List
from model import Product
import json
import requests


class FakeStoreAPI:
    _PRODUCT_PATH = "/products"

    def __init__(self, base_url):
        self._base_url = base_url

    @staticmethod
    def _parse_get_products_json(json):
        return Product(
            id=json["id"],
            title=json["title"],
            price=json["price"],
            image=json["image"],
            description=json["description"],
            category=json["category"],
            rating=json["rating"]["rate"],
            rating_count=json["rating"]["count"],
        )

    def get_products(self) -> List[Product]:
        products_response = requests.get(self._base_url + self._PRODUCT_PATH)
        if products_response.status_code == 200:
            products = [
                FakeStoreAPI._parse_get_products_json(json)
                for json in products_response.json()
            ]
            return products
        else:
            raise Exception(
                "Request failed with status code: " + str(products_response.status_code)
            )
