from api import FakeStoreAPI
from db import ProductStorage


if __name__ == "__main__":
    api = FakeStoreAPI("https://fakestoreapi.com")
    product_stogare = ProductStorage()
    products = [product for product in api.get_products()]
    product_stogare.save_products(products)
    print(f"{len(products)} products imported")
