# assignment 

This repo contains the assignments.  

`docs` folder contains the first assignment. 
root of this repo contains the second assignment.

## Running the assignment

To import the products from the API, after intsalling the `requirements.txt` run the following command:

```bash
    python src/import_products.py
```

To clean the database run the following command:

```bash
    rm remarkable.db
```

To preprocess the data run the following command:

```bash
    python src/preprocess_products.py
```
