# assignment - Case 1 

I would ask this to the commercial leader or the prouduct manager/owner, if there isn't any then I might be asking to the marketing leader etc. Some more technical questions would be asked to tech leads/developers.

## Understand the goal

1- Why do you need to understand product journey of customers? Targeted Advertisement? Marketing? Analytics/Insight?

Normally after getting this answer I would ask different questions regarding to the answer I get. I am assuming that the commercial leader wants to be able to know more about who our customers are by the anayltics/tracking data available or we can start collecting or join with first/third party data. The other assumption I would make is that this data can be used for advertisement/marketing and analytics/insights.

2- What is the current state of the tools commercial leader is using to serve their purpose? 

I am assuming that they use some tools that require some manual steps that is not very straightforward and not automated/standardized. 

## Understanding the current data/tools available

1- What kind of data is already available and how are we using the available data?

## Do we need more data? 

1- Do we need to collect more data/attributes or gather more data from other first party channels or from third party services?
- Potentially we can enrich the data we have via joining via other data we have or with third party data like more fine location data, add some machine learning predictions.

2- If we do not have the data/attributes we need for our use case, how can we collect more data, what are the possibilities? 

3- Do we collect data from all devices? Do they differ? 

4- Can we use more declared data?

## Data Quality

1- Is the data standardised with a schema/versioning? 

2- Is the data coming in a regular fashion? Is it automated? 

3- How clean is the data? Is there any deduplication/filtering invalid data etc.

4- How can we keep the client versions up-to-date so that we can ensure data quality?

## Do we have the necessary channels to activate the data?

1- What are the some examples of customer segments? How do we define a customer segment? Is there fixed amount of customer segments or do 

## What about privacy?

We can include a legal department/privacy department person.

1- Are we clear to the users about why are we collecting this data and how are we processing them?

2- Do we support GDPR/CCPA/other data regulations? 

3- Do we collect the data with consent? Do we support data take out, data opt-out and data deletion?



## High Level Design of the Data Platform


![Data Platform](assets/data-platform-high-level.svg "Data Platform High Level Design")
